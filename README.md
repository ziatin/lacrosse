Lacrosse
========

Lightweight cross-compiling tool for Rust in Docker.

> **Disclaimer**
> 
> I've created this set of Docker files and compile targets to satisfy my own needs while
> [`cross`](https://github.com/cross-rs/cross/) is broken and since
> [`dockcross`](https://github.com/dockcross/dockcross) does not provide support for Apple targets.
> 
> I would strongly suggest to use one of the mentioned tools in the long run. However, as a quick and straightforward
> solution for a small project this may suffice.

> **Note**
> 
> At the moment images are not built. Feel free to clone and set `CI_REGISTRY_PUSH_ALLOWED` to `true` (if you are 
> using GitLab).

Usage
-----

These are just basic instructions. Check [docker-compose.yml](docker-compose.yml) (example usage) and
[docker-compose.release.yml](docker-compose.lacrosse.yml) (Lacrosse base images) for details.

### Build OSXCross

If you are using this repository from source code then first build OSXCross:

```shell
MACOS_SDK_PATH=<path to your SDK> OSXCROSS_COMMIT=<OSXCross commit | upstream> docker-compose osxcross
```

Then build Lacrosse images:

To build base image for Apple provide path to your *packed* Xcode SDK. Check the 
[instructions](https://github.com/tpoechtrager/osxcross#packaging-the-sdk) on how to pack.

### Build for Linux targets

Set `TARGET_ARCH` and `TARGET_VARIANT` environment variables.

```shell
docker-compose build lacrosse-linux
docker-compose build build-linux
docker-compose run --rm build-linux
```

### Build for Apple targets

Assuming that `OSXCross` is already [built](#build-osxcross).

Set `TARGET_ARCH` and `TARGET_OS` environment variables.

```shell
docker-compose build lacrosse-apple
```

### Test

You can test builds with dummy Rust project.

For Apple targets:

```shell
docker-compose build build-apple
docker-compose run --rm build-apple
```

For Linux targets:

```shell
docker-compose build build-linux
docker-compose run --rm build-linux
```

Acknowledgements
----------------

I am using a free [Lacrosse icons created by smalllikeart - Flaticon](https://www.flaticon.com/free-icons/lacrosse), and I am
thankful for that. They require to post about this fact in social network, hope that GitLab counts ツ.

[Thomas Pöchtrager](https://github.com/tpoechtrager) did a great job creating
[OSXCross](https://github.com/tpoechtrager/osxcross), an outstanding toolchain for Apple targets.

[https://github.com/cross-rs]() managed to package `OSXCross` into Docker in 
[cross-toolchains](https://github.com/cross-rs/cross-toolchains), I've used a lot of their code. Hope they will soon fix
their magnum opus, the [`cross`](https://github.com/cross-rs/cross) tool, and make this project obsolete.

License
-------

> Here we simply comply with the suggested dual licensing according to
> [Rust API Guidelines](https://rust-lang.github.io/api-guidelines/about.html) (C-PERMISSIVE).

Licensed under either of

* Apache License, Version 2.0
  ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
* MIT license
  ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

Contribution
------------

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
