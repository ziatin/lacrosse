#!/usr/bin/env bash
set -e

TARGET_NAME="$(cat /var/log/lacrosse.rust-target.txt)"
LACROSSE_BUILD_STAGE="$(cat /var/lacrosse.build-cross-deps.stage.txt)"

echo "[ INFO ] Rust target: '${TARGET_NAME}'."
echo "[ INFO ] Cross-compile deps are defined in Docker stage: '${LACROSSE_BUILD_STAGE}'."
echo "[ INFO ] System info: '$(uname -a)'."

# Run requested command
"$@"
