#!/usr/bin/env bash

set -e

TARGET_NAME="$(cat /var/log/lacrosse.rust-target.txt)"
LACROSSE_BUILD_STAGE="$(cat /var/lacrosse.build-cross-deps.stage.txt)"

echo "[ INFO ] Rust target: '${TARGET_NAME}'."
echo "[ INFO ] Cross-compile deps are defined in Docker stage: '${LACROSSE_BUILD_STAGE}'."
echo "[ INFO ] System info: '$(uname -a)'."

# Check that target is not empty
compgen -c | grep "${TARGET_NAME}" 1>/dev/null || _ERR=true
if [ "${_ERR}" = "true" ]; then
    >&2 echo "[ERROR] Toolchain is not installed!"
    exit 1
fi

# extract our tools version. credit @0xdeafbeef.
tools=$(compgen -c | grep "${TARGET_NAME}")
version=$(echo "${tools}" | grep 'ar$' |  sed 's/'"${TARGET_NAME}"'//' | sed 's/-ar//')

# export our toolchain versions
envvar_suffix="${TARGET_NAME//-/_}"
upper_suffix=$(echo "${envvar_suffix}" | tr '[:lower:]' '[:upper:]')
tools_prefix="${TARGET_NAME}${version}"
declare -x AR_"${envvar_suffix}"="${tools_prefix}"-ar
declare -x CC_"${envvar_suffix}"="${tools_prefix}"-clang
declare -x CXX_"${envvar_suffix}"="${tools_prefix}"-clang++
declare -x CARGO_TARGET_"${upper_suffix}"_LINKER="${tools_prefix}"-clang

exec "$@"
