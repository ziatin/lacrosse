#!/usr/bin/env bash
set -e

# Set defaults
TARGET_ARCH="${TARGET_ARCH:-UNKNOWN}"
TARGET_OS="${TARGET_OS:-UNKNOWN}"
TARGET_VARIANT="${TARGET_VARIANT:-UNKNOWN}"
ADD_RUST_TARGET="${ADD_RUST_TARGET:=false}"

case "${TARGET_ARCH}" in
    arm64|aarch64) case "${TARGET_OS}" in
            linux) case "${TARGET_VARIANT}" in
                gnu) RUST_TARGET=aarch64-unknown-linux-gnu ;;
                musl) RUST_TARGET=aarch64-unknown-linux-musl ;;
                *) echo "[ ERROR ] Unknown variant: '${TARGET_VARIANT}'. Should be gnu|musl."; _ERROR=3 ;;
            esac ;;
            darwin) RUST_TARGET=aarch64-apple-darwin ;;
            *) echo "[ ERROR ] Unknown OS: '${TARGET_OS}'. Should be darwin|linux."; _ERROR=2 ;;
        esac ;;
    amd64|x86_64|x86-64) case "${TARGET_OS}" in
            linux) case "${TARGET_VARIANT}" in
                gnu) RUST_TARGET=x86_64-unknown-linux-gnu ;;
                musl) RUST_TARGET=x86_64-unknown-linux-musl ;;
                *) echo "[ ERROR ] Unknown variant: '${TARGET_VARIANT}'. Should be gnu|musl."; _ERROR=3 ;;
            esac ;;
            darwin) RUST_TARGET=x86_64-apple-darwin ;;
            *) echo "[ ERROR ] Unknown OS: '${TARGET_OS}'. Should be darwin|linux."; _ERROR=2 ;;
        esac ;;
    *) echo "[ ERROR ] Unknown architecture: '${_UNAME_M}'. Should be arm64|aarch64|amd64|x86_64|x86-64."; _ERROR=1 ;;
esac

if [ -z ${_ERROR+x} ]; then
  if [ "${ADD_RUST_TARGET}" = "true" ]; then
    echo "[ WARN ] Installing rust cross-compilation target: '${RUST_TARGET}'."
    rustup target add "${RUST_TARGET}"
  else
    echo "${RUST_TARGET}"
  fi
else
  echo "[ ERROR ] Invalid target specs. TARGET_ARCH = '$TARGET_ARCH', TARGET_OS = '$TARGET_OS', and TARGET_VARIANT = '$TARGET_VARIANT'."
  exit "$_ERROR"
fi
