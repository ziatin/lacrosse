#!/usr/bin/env bash
set -e

CARGO_BUILD_PARAMS="${CARGO_BUILD_PARAMS:-}"
TARGET_NAME="$(cat /var/log/lacrosse.rust-target.txt)"

if [ -z "${TARGET_NAME}" ]; then
  echo "[ ERROR ] Target is not specified. Lacrosse build is broken."
fi

echo "[ INFO ] Building for target: '${TARGET_NAME}'."
echo "[ INFO ] Cargo build parameters: '${CARGO_BUILD_PARAMS}'."

cargo build ${CARGO_BUILD_PARAMS} --target "${TARGET_NAME}"

echo "[ INFO ] Build for target '${TARGET_NAME}' completed."
