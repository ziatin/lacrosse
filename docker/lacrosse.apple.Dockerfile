# These args defined outside stages (can be used for naming images)
ARG TARGET_ARCH
ARG TARGET_OS
ARG OSXCROSS_IMAGE
ARG OSXCROSS_TAG

FROM ${OSXCROSS_IMAGE}:${OSXCROSS_TAG} as build-base

FROM build-base as build-cross-deps-aarch64-apple-darwin

FROM build-base as build-cross-deps-x86_64-apple-darwin

FROM build-cross-deps-${TARGET_ARCH}-apple-${TARGET_OS} as build-rustup-add-target

# These arguments define cross-compilation target
ARG TARGET_ARCH
ARG TARGET_OS
ARG TARGET_VARIANT

# Setup script that calculates appropriate rust cross-compile target
COPY ../scripts/Docker.build.rust-target.sh /usr/local/bin/lacrosse.build.rust-target.sh
RUN chmod +x /usr/local/bin/lacrosse.build.rust-target.sh

# Install appropriate rust cross-compilation target
RUN ADD_RUST_TARGET=true lacrosse.build.rust-target.sh

# Save build stage info
RUN echo build-cross-deps-${TARGET_ARCH}-apple-${TARGET_OS} > /var/lacrosse.build-cross-deps.stage.txt
# Save rustup target info
RUN lacrosse.build.rust-target.sh > /var/log/lacrosse.rust-target.txt

FROM build-rustup-add-target as release

# Entrypoint script
COPY ../scripts/Docker.build.apple.entrypoint.sh /usr/local/bin/Docker.entrypoint.sh
RUN chmod +x /usr/local/bin/Docker.entrypoint.sh

# Cargo build script
COPY ./scripts/Docker.build.cmd.sh /usr/local/bin/Docker.cmd.sh
RUN chmod +x /usr/local/bin/Docker.cmd.sh

# Setup entrypoint and default command
ENTRYPOINT ["/usr/local/bin/Docker.entrypoint.sh"]
CMD ["/usr/local/bin/Docker.cmd.sh"]
