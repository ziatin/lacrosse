#
# OSXCross toolchain for cross-compiling Rust to Apple targets
#
# Based on official repository suggestions: https://github.com/tpoechtrager/osxcross
# and `cross-toolchains` https://github.com/cross-rs/cross-toolchains/ implementation.
#

ARG RUST_TAG=1

FROM docker.io/ubuntu:23.04 as build-osxcross-base

# Install basic dependencies
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        ca-certificates \
        git \
    && rm -rf /var/lib/apt/lists/*

# Install build tools
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        gcc \
        g++ \
        make \
        cmake \
        patch \
        xz-utils \
        python3 \
    && rm -rf /var/lib/apt/lists/*

# Install CLang and libs
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        clang \
        libmpc-dev \
        libmpfr-dev \
        libgmp-dev \
        libssl-dev \
        libxml2-dev \
        zlib1g-dev \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /opt

FROM build-osxcross-base as build-osxcross

# Clone OSXCross repository
RUN git clone https://github.com/tpoechtrager/osxcross --depth 1 /tmp/osxcross

ARG OSXCROSS_COMMIT=upstream
RUN if [ "${OSXCROSS_COMMIT}" != "upstream" ]; then \
        cd /tmp/osxcross; \
        git fetch --depth=1 origin "${OSXCROSS_COMMIT}"; \
        git checkout "${OSXCROSS_COMMIT}"; \
    fi

# Copy Xcode SDK tarball
ARG MACOS_SDK_PATH
COPY "${MACOS_SDK_PATH}" /tmp/osxcross/tarballs/

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        python3 \
    && rm -rf /var/lib/apt/lists/*

# Build OSXCross
RUN mkdir -p /opt/osxcross && \
    cd /tmp/osxcross && \
    TARGET_DIR=/opt/osxcross UNATTENDED=yes OSX_VERSION_MIN=10.7 ./build.sh

FROM docker.io/rust:${RUST_TAG} as release

# Copy compiled OSXCross
COPY --from=build-osxcross /opt/osxcross/ /opt/osxcross/

# Install build utils required by OSXCross toolchain
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        gcc \
        g++ \
        clang \
    && rm -rf /var/lib/apt/lists/*

# Set proper `PATH`
ENV PATH=/opt/osxcross/bin:$PATH
