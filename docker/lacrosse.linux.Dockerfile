# These args defined outside stages (can be used for naming images)
ARG TARGET_ARCH
ARG TARGET_VARIANT

FROM docker.io/rust:1 as build-base

# Install general system dependencies
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        git \
    && rm -rf /var/lib/apt/lists/*

FROM build-base as build-cross-deps-aarch64-linux-gnu

RUN dpkg --add-architecture arm64
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        g++-aarch64-linux-gnu \
        libc6-dev-arm64-cross \
    && rm -rf /var/lib/apt/lists/*

FROM build-cross-deps-aarch64-linux-gnu as build-cross-deps-aarch64-linux-musl

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        musl-tools:arm64 musl-dev:arm64 \
    && rm -rf /var/lib/apt/lists/*

FROM build-base as build-cross-deps-x86_64-linux-gnu

RUN dpkg --add-architecture amd64
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        g++-x86-64-linux-gnu \
        libc6-dev-amd64-cross \
    && rm -rf /var/lib/apt/lists/*

FROM build-cross-deps-x86_64-linux-gnu as build-cross-deps-x86_64-linux-musl

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        musl-tools:amd64 musl-dev:amd64 \
    && rm -rf /var/lib/apt/lists/*

FROM build-cross-deps-${TARGET_ARCH}-linux-${TARGET_VARIANT} as build-rustup-add-target

# These arguments define cross-compilation target
ARG TARGET_ARCH
ARG TARGET_OS
ARG TARGET_VARIANT

# Setup script that calculates appropriate rust cross-compile target
COPY ../scripts/Docker.build.rust-target.sh /usr/local/bin/lacrosse.build.rust-target.sh
RUN chmod +x /usr/local/bin/lacrosse.build.rust-target.sh

# Install appropriate rust cross-compilation target
RUN ADD_RUST_TARGET=true lacrosse.build.rust-target.sh

# Save build stage info
RUN echo build-cross-deps-${TARGET_ARCH}-apple-${TARGET_OS} > /var/lacrosse.build-cross-deps.stage.txt
# Save rustup target info
RUN lacrosse.build.rust-target.sh > /var/log/lacrosse.rust-target.txt

FROM build-rustup-add-target as release

# Cargo config
COPY ../.cargo/config.toml /usr/local/cargo/config.toml

# Entrypoint script
COPY ../scripts/Docker.build.linux.entrypoint.sh /usr/local/bin/Docker.entrypoint.sh
RUN chmod +x /usr/local/bin/Docker.entrypoint.sh

# Cargo build script
COPY ./scripts/Docker.build.cmd.sh /usr/local/bin/Docker.cmd.sh
RUN chmod +x /usr/local/bin/Docker.cmd.sh

# Setup entrypoint and default command
ENTRYPOINT ["/usr/local/bin/Docker.entrypoint.sh"]
CMD ["/usr/local/bin/Docker.cmd.sh"]
