# These args defined outside stages (can be used for naming images)
ARG TARGET_ARCH
ARG TARGET_VARIANT

FROM lacrosse-lacrosse-${TARGET_ARCH}-unknown-linux-${TARGET_VARIANT}:local

ARG CARGO_BUILD_PARAMS=""
ENV CARGO_BUILD_PARAMS=${CARGO_BUILD_PARAMS}

# Create build script
COPY ./scripts/Docker.build.cmd.sh /usr/local/bin/Docker.cmd.sh
RUN chmod +x /usr/local/bin/Docker.cmd.sh

# Create directories
RUN mkdir -p /work/target

# Switch to working directory
WORKDIR /work

COPY .. .

CMD ["/usr/local/bin/Docker.cmd.sh"]
